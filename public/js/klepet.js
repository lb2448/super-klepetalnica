// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
  this.nadimki = {};
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  besede.shift();
  var sporocilo = false;

  switch (ukaz) {
    case 'pridruzitev':
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'krcni':
    case 'zasebno':
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (ukaz == 'krcni') {
        parametri[3] = '☜';
      }
      if (parametri) {
        this.socket.emit('sporocilo', {
          vzdevek: parametri[1],
          besedilo: parametri[3]
        });
        sporocilo = `(zasebno za ${parametri[1]}): ${parametri[3]}`;
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'barva':
      let barva = besede.join(" ");
      let elems = $('#kanal, #sporocila');
      let prevBarva = elems.css('background-color');
      elems.css('background-color', barva);
      if(prevBarva === elems.css('background-color')){
        sporocilo = 'Nepravilno vnešena barva: ' + barva;
      }
      break;
    case 'preimenuj':
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      console.log(parametri);
      if (parametri.length == 5) {
        if (parametri[1] == "") {
          delete this.nadimki[parametri[1]];
        }else{
          this.nadimki[parametri[1]] = parametri[3];
        }
        sporocilo = '(nadimek za ' + besede[0] + '): ' + besede[1];
      } else {
        sporocilo = 'Neznan ukaz';
      }
    break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};
